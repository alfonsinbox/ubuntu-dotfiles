#!/usr/bin/env bash

i3status | while :
do
    read line
    home_count=`ls -l $HOME | wc -l`
    echo "CNT: $home_count | $line" || exit 1
done
