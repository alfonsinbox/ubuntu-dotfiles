set expandtab
set shiftwidth=2
set tabstop=2
set autoindent
set smartindent
set mouse=a
set ignorecase

set number
set relativenumber

syntax on
colorscheme nord
autocmd CursorHold * update
